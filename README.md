# Scrum Score

This is an application to aid team members for scoring scrum points.

## Installation
Use the package manager yarn to install.

```bash
yarn
```

## Usage

```bash
yarn start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
